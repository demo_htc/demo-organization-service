package com.htc.ea.organizationservice.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

public class OrganizationRequest {
	@NotNull
	@Length(min=1,max=20)
	private String name;
	@NotNull
	@Length(min=1,max=20)
	private String address;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OrganizationRequest [");
		if (name != null) {
			builder.append("name=");
			builder.append(name);
			builder.append(", ");
		}
		if (address != null) {
			builder.append("address=");
			builder.append(address);
		}
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
