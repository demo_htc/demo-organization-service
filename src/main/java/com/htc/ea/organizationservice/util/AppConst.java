package com.htc.ea.organizationservice.util;

public class AppConst {
	public static final String MSG_ERROR = "Error";
	public static final String MSG_DESCRIPTION = "Description";
	public static final String MSG_ORGANIZATION_FOUND = "organizacion encontrada";
	public static final String MSG_ORGANIZATION_NOT_FOUND = "no se encontro organizacion con id: ";
	public static final String MSG_ORGANIZATION_ERROR_FOUND = "Ocurrio un error al intentar buscar organizacion con id: ";
	
}
