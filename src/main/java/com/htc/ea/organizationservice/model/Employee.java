package com.htc.ea.organizationservice.model;

public class Employee {

	private Long id;
	private Long idOrganization;
	private Long idDepartment;
	private String name;
	private int age;
	private String position;

	public Employee() {

	}

	public Employee(Long id, Long idOrganization, Long idDepartment, String name, int age, String position) {
		super();
		this.id = id;
		this.idOrganization = idOrganization;
		this.idDepartment = idDepartment;
		this.name = name;
		this.age = age;
		this.position = position;
	}	

	public Long getIdOrganization() {
		return idOrganization;
	}

	public void setIdOrganization(Long idOrganization) {
		this.idOrganization = idOrganization;
	}

	public Long getIdDepartment() {
		return idDepartment;
	}

	public void setIdDepartment(Long idDepartment) {
		this.idDepartment = idDepartment;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Employee [");
		if (id != null) {
			builder.append("id=");
			builder.append(id);
			builder.append(", ");
		}
		if (idOrganization != null) {
			builder.append("idOrganization=");
			builder.append(idOrganization);
			builder.append(", ");
		}
		if (idDepartment != null) {
			builder.append("idDepartment=");
			builder.append(idDepartment);
			builder.append(", ");
		}
		if (name != null) {
			builder.append("name=");
			builder.append(name);
			builder.append(", ");
		}
		builder.append("age=");
		builder.append(age);
		builder.append(", ");
		if (position != null) {
			builder.append("position=");
			builder.append(position);
		}
		builder.append("]");
		return builder.toString();
	}
	
	
	
	
}
