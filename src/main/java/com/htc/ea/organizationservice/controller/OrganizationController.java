package com.htc.ea.organizationservice.controller;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import javax.validation.Valid;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.htc.ea.organizationservice.dto.OrganizationRequest;
import com.htc.ea.organizationservice.model.Organization;
import com.htc.ea.organizationservice.service.OrganizationService;


@RestController
public class OrganizationController {
	
	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Value("${server.port}")
	private int serverPort;
	
	@Autowired
	private OrganizationService organizationService;
	@Autowired
	private Mapper mapper;
	
	@PostMapping(value="/",produces=MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Organization> save(@Valid @RequestBody OrganizationRequest request) {
		showIpAndPort("POST / "+request.toString());
		return organizationService.save(mapper.map(request, Organization.class));
	}
	
	@GetMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Organization> findById(@PathVariable("id") Long id) {
		showIpAndPort("GET / "+String.valueOf(id));
		return organizationService.findById(id);
	}
	
	@GetMapping(value="/",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Organization>> findAll() {
		showIpAndPort("GET /");
		return organizationService.findAll();
	}
	
	@GetMapping(value="/{id}/departments",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Organization> findByIdWithDepartments(@PathVariable("id") Long id) {
		showIpAndPort("GET /"+String.valueOf(id)+"/departments");
		return organizationService.findByIdWithDepartments(id);
	}
	
	@GetMapping(value="/{id}/departments/employees",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Organization> findByIdWithDepartmentsAndEmployees(@PathVariable("id") Long id) {
		showIpAndPort("GET /"+String.valueOf(id)+"/department/employees");
		return organizationService.findByIdWithDepartmentsWithEmployees(id);
	}
	
	@GetMapping(value="/{id}/employees",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Organization> findByIdWithEmployees(@PathVariable("id") Long id) {
		showIpAndPort("GET /"+String.valueOf(id)+"/employees");
		return organizationService.findByIdWithEmployees(id);
	}
	
	@GetMapping(value="/{id}/exist",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> organizationExist(@PathVariable("id") Long id){
		showIpAndPort("GET /"+String.valueOf(id)+"/exist");
		return organizationService.exist(id);
	}
	
	/**
	 * muestra en logs informacion de la peticion
	 * @param inf informacion del request de la peticion
	 */
	private void showIpAndPort(String inf) {
		try {
			StringBuilder builder = new StringBuilder();
			builder.append("IP:PORT ");
			builder.append(InetAddress.getLocalHost().getHostAddress());
			builder.append(":");
			builder.append(serverPort);
			builder.append(" REQUEST ");
			builder.append(inf);
			log.info(builder.toString());
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
}
