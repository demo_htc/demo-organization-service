package com.htc.ea.organizationservice.client;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.htc.ea.organizationservice.model.Employee;

/**
 * Interfaz de definicion de metodos de consumo de API employee 
 * consulta a microservicio de empleados y obtiene diversa informacion
 * @author htc
 *
 */
public interface EmployeeApiClient {
	
	/**
	 * consulta al microservicio employee
	 * retorna lista de empleados bajo el id de organizacion asociado
	 * contiene una funcion fallback en el caso de que no se pueda conectar al microservicio
	 * @param id organization
	 * @return response entity con headers y lista de empleados en el body
	 */
	public ResponseEntity<List<Employee>> findAllEmployeesByIdOrganization(Long id); 
}
