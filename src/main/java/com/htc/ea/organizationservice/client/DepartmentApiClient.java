package com.htc.ea.organizationservice.client;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.htc.ea.organizationservice.model.Department;

/**
 * Interfaz de definicion de metodos de consumo de API department
 * consulta a microservicio de departamento y obtiene diversa informacion
 * @author htc
 *
 */
public interface DepartmentApiClient {
	/**
	 * consulta al microservicio department
	 * retorna lista de departamentos bajo el id de organizacion asociado
	 * contiene una funcion fallback en el caso de que no se pueda conectar al microservicio
	 * @param id organization
	 * @return response entity con headers y lista de department en el body
	 */
	public ResponseEntity<List<Department>> findAllDepartmentsByIdOrganization(Long id);
	
	/**
	 * consulta al microservicio department
	 * retorna lista de departamentos con empleados bajo el id de organizacion asociado
	 * contiene una funcion fallback en el caso de que no se pueda conectar al microservicio
	 * @param id organization
	 * @return response entity con headers y lista de department en el body
	 */
	public ResponseEntity<List<Department>> findAllDepartmentsByIdOrganizationWithEmployees(Long id);
}
